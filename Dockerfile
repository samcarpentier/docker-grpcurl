FROM quay.io/datawire/grpcurl:latest

ENTRYPOINT [ "/go/bin/grpcurl" ]
